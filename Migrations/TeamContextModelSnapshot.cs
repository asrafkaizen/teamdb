﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TeamMembersDB.Data;

namespace TeamMembersDB.Migrations
{
    [DbContext(typeof(TeamContext))]
    partial class TeamContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TeamMembersDB.Model.Department", b =>
                {
                    b.Property<int>("DepartmentID");

                    b.Property<int>("Loading");

                    b.Property<int>("TeamID");

                    b.Property<string>("Title")
                        .HasMaxLength(50);

                    b.HasKey("DepartmentID");

                    b.HasIndex("TeamID");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("TeamMembersDB.Model.DepartmentAssignment", b =>
                {
                    b.Property<int>("DepartmentID");

                    b.Property<int>("SupervisorID");

                    b.HasKey("DepartmentID", "SupervisorID");

                    b.HasIndex("SupervisorID");

                    b.ToTable("DepartmentAssignment");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Enrollment", b =>
                {
                    b.Property<int>("EnrollmentID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DepartmentID");

                    b.Property<int?>("Level");

                    b.Property<int>("MemberID");

                    b.HasKey("EnrollmentID");

                    b.HasIndex("DepartmentID");

                    b.HasIndex("MemberID");

                    b.ToTable("Enrollment");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Member", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("EnrollmentDate");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.HasKey("ID");

                    b.ToTable("Member");
                });

            modelBuilder.Entity("TeamMembersDB.Model.RegionAssignment", b =>
                {
                    b.Property<int>("SupervisorID");

                    b.Property<string>("Location")
                        .HasMaxLength(50);

                    b.HasKey("SupervisorID");

                    b.ToTable("RegionAssignment");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Supervisor", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FirstMidName")
                        .IsRequired()
                        .HasColumnName("FirstName")
                        .HasMaxLength(50);

                    b.Property<DateTime>("HireDate");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("ID");

                    b.ToTable("Supervisor");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Team", b =>
                {
                    b.Property<int>("TeamID")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Budget")
                        .HasColumnType("money");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<DateTime>("StartDate");

                    b.Property<int?>("SupervisorID");

                    b.HasKey("TeamID");

                    b.HasIndex("SupervisorID");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Department", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Team", "Team")
                        .WithMany("Department")
                        .HasForeignKey("TeamID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TeamMembersDB.Model.DepartmentAssignment", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Department", "Department")
                        .WithMany("DepartmentAssignment")
                        .HasForeignKey("DepartmentID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TeamMembersDB.Model.Supervisor", "Supervisor")
                        .WithMany("DepartmentAssignments")
                        .HasForeignKey("SupervisorID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TeamMembersDB.Model.Enrollment", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Department", "Department")
                        .WithMany("Enrollments")
                        .HasForeignKey("DepartmentID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TeamMembersDB.Model.Member", "Member")
                        .WithMany("Enrollments")
                        .HasForeignKey("MemberID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TeamMembersDB.Model.RegionAssignment", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Supervisor", "Supervisor")
                        .WithOne("RegionAssignment")
                        .HasForeignKey("TeamMembersDB.Model.RegionAssignment", "SupervisorID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TeamMembersDB.Model.Team", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Supervisor", "Administrator")
                        .WithMany()
                        .HasForeignKey("SupervisorID");
                });
        }
    }
}
