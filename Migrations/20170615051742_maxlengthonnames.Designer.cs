﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TeamMembersDB.Data;

namespace TeamMembersDB.Migrations
{
    [DbContext(typeof(TeamContext))]
    [Migration("20170615051742_maxlengthonnames")]
    partial class maxlengthonnames
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TeamMembersDB.Model.Department", b =>
                {
                    b.Property<int>("DepartmentID");

                    b.Property<int>("Loading");

                    b.Property<string>("Title");

                    b.HasKey("DepartmentID");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Enrollment", b =>
                {
                    b.Property<int>("EnrollmentID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DepartmentID");

                    b.Property<int?>("Level");

                    b.Property<int>("MemberID");

                    b.HasKey("EnrollmentID");

                    b.HasIndex("DepartmentID");

                    b.HasIndex("MemberID");

                    b.ToTable("Enrollment");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Member", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<DateTime>("EnrollmentDate");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<string>("Phone");

                    b.HasKey("ID");

                    b.ToTable("Member");
                });

            modelBuilder.Entity("TeamMembersDB.Model.Enrollment", b =>
                {
                    b.HasOne("TeamMembersDB.Model.Department", "Department")
                        .WithMany("Enrollments")
                        .HasForeignKey("DepartmentID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TeamMembersDB.Model.Member", "Member")
                        .WithMany("Enrollments")
                        .HasForeignKey("MemberID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
