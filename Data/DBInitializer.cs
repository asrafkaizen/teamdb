﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamMembersDB.Model;

namespace TeamMembersDB.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TeamContext context)
        {
            context.Database.EnsureCreated();

            // Look for any Members.
            if (context.Members.Any())
            {
                return;   // DB has been seeded
            }

            var members = new Member[]
            {
            new Member{Name="Omar",Email="Omar@gmail",Phone="011-325671819",EnrollmentDate=DateTime.Parse("2012-09-01")},
            new Member{Name="Uthman",Email="Uthman@yahoomail",Phone="019-325671819",EnrollmentDate=DateTime.Parse("2012-09-01")},
            new Member{Name="Siti",Email="Siti@gmail",Phone="012-325671819",EnrollmentDate=DateTime.Parse("2013-09-01")},
            new Member{Name="Kalsom",Email="Kalsom@gmail",Phone="016-325671819",EnrollmentDate=DateTime.Parse("2014-09-01")},
            new Member{Name="Bedah",Email="Surgery@gmail",Phone="010-325671819",EnrollmentDate=DateTime.Parse("2015-09-01")},
            new Member{Name="Joyah",Email="Justice@gmail",Phone="013-325671819",EnrollmentDate=DateTime.Parse("2015-09-01")},
            new Member{Name="Tuah",Email="tuah@gmail",Phone="014-325671819",EnrollmentDate=DateTime.Parse("2014-09-01")},
            new Member{Name="Mamat",Email="xde email",Phone="019-9932548",EnrollmentDate=DateTime.Parse("2013-09-01")}
            };
            foreach (Member s in members)
            {
                context.Members.Add(s);
            }
            context.SaveChanges();
            
            var supervisors = new Supervisor[]
            {
                new Supervisor { FirstMidName = "Harith",     LastName = "Zulkifli",
                    HireDate = DateTime.Parse("1995-03-11") },
                new Supervisor { FirstMidName = "Fadi",    LastName = "Fakhouri",
                    HireDate = DateTime.Parse("2002-07-06") },
                new Supervisor { FirstMidName = "Mahmud",   LastName = "Kassim",
                    HireDate = DateTime.Parse("1998-07-01") },
                new Supervisor { FirstMidName = "Siti", LastName = "Kapoor",
                    HireDate = DateTime.Parse("2001-01-15") },
                new Supervisor { FirstMidName = "Zabedah",   LastName = "Jumaat",
                    HireDate = DateTime.Parse("2004-02-12") }
            };

            foreach (Supervisor i in supervisors)
            {
                context.Supervisors.Add(i);
            }
            context.SaveChanges();

            var teams = new Team[]
            {
                new Team { Name = "Human Resources",     Budget = 350000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    SupervisorID  = supervisors.Single( i => i.LastName == "Zulkifli").ID },
                new Team { Name = "Technology", Budget = 100000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    SupervisorID  = supervisors.Single( i => i.LastName == "Fakhouri").ID },
                new Team { Name = "Engineering", Budget = 350000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    SupervisorID  = supervisors.Single( i => i.LastName == "Kassim").ID },
                new Team { Name = "Economics",   Budget = 100000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    SupervisorID  = supervisors.Single( i => i.LastName == "Kapoor").ID }
            };

            foreach (Team d in teams)
            {
                context.Teams.Add(d);
            }
            context.SaveChanges();

            var Departments = new Department[]
            {
            new Department{DepartmentID=1050,Title="Chemistry",Loading=3 ,
                TeamID = teams.Single( s => s.Name == "Engineering").TeamID
                    },
            new Department { DepartmentID = 4022, Title = "Microeconomics", Loading = 3 ,
                TeamID = teams.Single( s => s.Name == "Economics").TeamID
                        },
            new Department { DepartmentID = 4041, Title = "Macroeconomics", Loading = 3 ,
                TeamID = teams.Single( s => s.Name == "Economics").TeamID
                        },
            new Department { DepartmentID = 1045, Title = "Software", Loading = 4 ,
                TeamID = teams.Single( s => s.Name == "Technology").TeamID
                        },
            new Department { DepartmentID = 3141, Title = "Hardware", Loading = 4 ,
                TeamID = teams.Single( s => s.Name == "Technology").TeamID
                        },
            new Department { DepartmentID = 2021, Title = "Management", Loading = 3 ,
                TeamID = teams.Single( s => s.Name == "Human Resources").TeamID
                        },
            new Department { DepartmentID = 2042, Title = "Leadership", Loading = 4 ,
                TeamID = teams.Single( s => s.Name == "Human Resources").TeamID
                        }
            };
            foreach (Department c in Departments)
            {
                context.Departments.Add(c);
            }
            context.SaveChanges();
            
            var enrollments = new Enrollment[]
            {
            new Enrollment{MemberID=1,DepartmentID=1050,Level=Level.A},
            //new Enrollment {
            //        MemberID = members.Single(s => s.Name == "Omar").ID,
            //        DepartmentID = Departments.Single(c => c.Title == "Chemistry" ).DepartmentID,
            //        Level = Level.A
            //    },
            new Enrollment{MemberID=1,DepartmentID=4022,Level=Level.C},
            new Enrollment{MemberID=1,DepartmentID=4041,Level=Level.B},
            new Enrollment{MemberID=2,DepartmentID=1045,Level=Level.B},
            new Enrollment{MemberID=2,DepartmentID=3141,Level=Level.F},
            new Enrollment{MemberID=2,DepartmentID=2021,Level=Level.F},
            new Enrollment{MemberID=3,DepartmentID=1050},
            new Enrollment{MemberID=4,DepartmentID=1050},
            new Enrollment{MemberID=4,DepartmentID=4022,Level=Level.F},
            new Enrollment{MemberID=5,DepartmentID=4041,Level=Level.C},
            new Enrollment{MemberID=6,DepartmentID=1045},
            new Enrollment{MemberID=7,DepartmentID=3141,Level=Level.A},
            };
            foreach (Enrollment e in enrollments)
            {
                var enrollmentInDataBase = context.Enrollments.Where(
                    s =>
                            s.Member.ID == e.MemberID &&
                            s.Department.DepartmentID == e.DepartmentID).SingleOrDefault();
                if (enrollmentInDataBase == null)
                {
                    context.Enrollments.Add(e);
                }
            }
            context.SaveChanges();

            var departmentAssignments = new DepartmentAssignment[]
            {
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Chemistry" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Kassim").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Chemistry" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Kassim").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Microeconomics" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Kapoor").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Macroeconomics" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Kapoor").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Software" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Fakhouri").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Hardware" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Fakhouri").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Management" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Zulkifli").ID
                    },
                new DepartmentAssignment {
                    DepartmentID = Departments.Single(c => c.Title == "Leadership" ).DepartmentID,
                    SupervisorID = supervisors.Single(i => i.LastName == "Zulkifli").ID
                    },
            };       
            foreach (DepartmentAssignment ci in departmentAssignments)
            {
                context.DepartmentAssignments.Add(ci);
            }
            //context.SaveChanges();

            var regionAssignments = new RegionAssignment[]
            {
                new RegionAssignment {
                    SupervisorID = supervisors.Single( i => i.LastName == "Fakhouri").ID,
                    Location = "Northern" },
                new RegionAssignment {
                    SupervisorID = supervisors.Single( i => i.LastName == "Kassim").ID,
                    Location = "Central" },
                new RegionAssignment {
                    SupervisorID = supervisors.Single( i => i.LastName == "Kapoor").ID,
                    Location = "Southern" },
            };

            foreach (RegionAssignment o in regionAssignments)
            {
                context.RegionAssignments.Add(o);
            }
            context.SaveChanges();
            
        }
    }
}