﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamMembersDB.Model;

namespace TeamMembersDB.Data
{
    public class TeamContext : DbContext
    {
            public TeamContext(DbContextOptions<TeamContext> options) : base(options)
            {
            }

            public DbSet<Department> Departments { get; set; }
            public DbSet<Enrollment> Enrollments { get; set; }
            public DbSet<Member> Members { get; set; }
            public DbSet<Team> Teams { get; set; }
            public DbSet<Supervisor> Supervisors { get; set; }
            public DbSet<RegionAssignment> RegionAssignments { get; set; }
            public DbSet<DepartmentAssignment> DepartmentAssignments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");
            modelBuilder.Entity<Member>().ToTable("Member");
            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Supervisor>().ToTable("Supervisor");
            modelBuilder.Entity<RegionAssignment>().ToTable("RegionAssignment");
            modelBuilder.Entity<DepartmentAssignment>().ToTable("DepartmentAssignment");

            modelBuilder.Entity<DepartmentAssignment>()
                .HasKey(c => new { c.DepartmentID, c.SupervisorID });
        }

    }
}