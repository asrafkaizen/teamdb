﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TeamMembersDB.Model
{
    public class Member
    {
        public int ID { get; set; }
        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 1)]
        public string Name { get; set; }
        [StringLength(50, ErrorMessage = "Nameame cannot be longer than 50 characters.")]

        [Required]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        //[Display(Name = "Full Name")]
        //public string FullName
        //{
        //    get
        //    {
        //        return LastName + ", " + FirstMidName;
        //    }
        //}

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EnrollmentDate { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }
       
    }
}
