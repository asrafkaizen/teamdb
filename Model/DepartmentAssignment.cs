﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamMembersDB.Model
{
    public class DepartmentAssignment
    {
        public int SupervisorID { get; set; }
        public int DepartmentID { get; set; }
        public Supervisor Supervisor { get; set; }
        public Department Department { get; set; }
    }
}