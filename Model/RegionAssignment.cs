﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeamMembersDB.Model
{
    public class RegionAssignment
    {
        [Key]
        public int SupervisorID { get; set; }
        [StringLength(50)]
        [Display(Name = "Office Location")]
        public string Location { get; set; }

        public Supervisor Supervisor { get; set; }
    }
}