﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TeamMembersDB.Models.TeamViewModels
{
    public class EnrollmentDateGroup
    {
        [DataType(DataType.Date)]
        public DateTime? EnrollmentDate { get; set; }

        public int MemberCount { get; set; }
    }
}