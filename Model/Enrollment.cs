﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeamMembersDB.Model
{
    public enum Level
    {
        A, B, C, D, E, F
    }
    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        public int DepartmentID { get; set; }
        public int MemberID { get; set; }
        [DisplayFormat(NullDisplayText = "Level unspecified")]
        public Level? Level { get; set; }

        public Department Department { get; set; }
        public Member Member { get; set; }
    
    }
}
