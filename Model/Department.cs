﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TeamMembersDB.Model
{
    public class Department
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Number")]
        public int DepartmentID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        [Range(0, 5)]
        public int Loading { get; set; }

        public int TeamID { get; set; }

        public Team Team { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }
        public ICollection<DepartmentAssignment> DepartmentAssignment { get; set; }
      
    }
}